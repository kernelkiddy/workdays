"use strict";
const moment = require('moment-timezone');

function isWeekend(d) {
    console.log(`day=${d.day()} weekend=${d.day() == 6 || d.day() == 0}`);
    //return(d.day() == 6 || d.day() == 0);
    return([6,0].indexOf(d.day())!=-1);
}

function isHolliday(d) {
    const hdays=["2016-01-01","2016-01-02"];
    return(d.format("YYYY-MM-DD") in hdays);
    //return(false);
}

function isBlackWeekend(d) {
    const bdays=["2016-09-18"];
    return(d.format("YYYY-MM-DD") in bdays);
}

function isWorkday(d) {
//cosumes date, returns if it is a working day
    return(!(isWeekend(d)||isHolliday(d))||isBlackWeekend(d));
}

//let DaysInYear = moment;
const year = 2016;
//const oneDay=24*60*60*1000;

//setting to the first day of the year
//let d=moment.utc([year,0,1]);
//let EOY = d.endOf("year"); //last day of a year
//console.log(`End of ${d.format('YYYY')} year is ${EOY.format('YYYY-MM-DD')}`);

//loop all 12 months
for(let curMonth = 0; curMonth<12; curMonth++) {
  
    let workdays = 0; //current workday number
    let wdn=1; //current number for Cognos
    let d = moment.utc([year, curMonth, 1]);
    let DIM = d.daysInMonth(); //days in current month
    console.log(`${d}=====> Month=${curMonth} of ${year} has ${DIM} days in it`);
    //loop all days in month
    for(let i=1; i<=DIM; i++){
        if(isWorkday(moment.utc([year,curMonth,i]) )) {
            workdays++;
            wdn = workdays;
        }

        //console.log(`DIM=${DIM}, wdn=${wdn}, i=${i}`);
    }
    console.log(`............Total workdays for ${curMonth} =${workdays}`);

}


console.log(`${moment.utc([2016, 8, 17])} isWeekend = ${isWeekend(moment.utc([2016, 8, 17]))}`);   