"use strict";

const moment = require('moment-timezone');

import {p1,p2} from "./ppp";
//console.log(p1);

// Загружаем модуль файловой системы
let fs = require('fs');

// Считываем содержание файла в память
fs.readFile('log.txt', 'utf8', (err, logData)=> {
 
    // Если возникла ошибка, мы кидаем исключение
    // и программа заканчивается
    if (err) throw err;
 
    // logData это объект типа Buffer, переводим в строку
    let text = logData.toString();
    let results = {};
 
    // Разбиваем текст на массив из строчек
    let lines = text.split('\n');
 
    lines.forEach(function(line) {
        console.log(`line is:${line}`); 
        if(line){  
            let parts = line.split(' ');
            let curDate = parts[0];
            let numInMonth= parts[1];
            let maxMonth= parts[2];
            
            curDate=(curDate ? curDate : "NO_DATE_FOUND");
            let d= new Date(curDate);
            console.log(`date is ${moment(d).format("YYYY-MM-DD")}`);
        }
    });

});

const sentences = [
    { subject: 'JavaScript', verb: 'is', object: 'great' },
    { subject: 'Elephants', verb: 'are', object: 'large' },
];

function say({ subject, verb, object }) {
    // es6 feature: template strings
    console.log(`${subject} ${verb} ${object}`);
}
// es6 feature: for..of
for(let s of sentences) {
    say(s);
}

console.log("done!");

